# 1. Get running on local machine

This is how you set up the project for development.
This was tested working in python 3.10. I would reccomend using conda manage the environment. 

Create conda envionment
```bash
conda create -n pwm_visualiser python=3.10
```
Activate the environment 
```
conda activate pwm_visualiser
```
cd into the sources folder. Install the required packages using the conda-created python installation
```
python -m pip install -r requirements.txt
```
All the required dependancies should now be installed. 
Now you should be able to run the web-app:
```
python main.py
```
Open a browser and go to http://127.0.0.1:8050/, the webpage will be running there. The magic of dash is that you can make changes to the source code while the webpage is running and the page will update (on a refresh)
# 2. Setup the container for deployment
The webpage backend uses a package called gunicorn. If you look in the dockerfile, the server is started using this command:
```
CMD gunicorn --bind :$PORT --log-level info  --workers 8 --timeout 0 main:server
```
You can access settings such as number of processes per server core by changing --workers argument. See [gunicorn doc](https://docs.gunicorn.org/en/latest/run.html) for more settings

 Ensure the docker daemon is running and then build the container. First cd into the source folder, and then execute this line
```
docker build -f Dockerfile -t pwm_visualiser_image .
```
Now you can run the docker container.
## 3. Running the webserver
Execute this command to start a container with the webpage
```
docker run --env PORT=80 -p 8050:80 pwm_visualiser_image
```
If you go to http://localhost:8050/ you should now be able to access the webpage from the host machine 


You specify the server socket for the container using --env PORT environmental varaible. You can then link the containers port with one on the host machine using the -p argument, eg -p 8050:80 will map port 80 on the container with 8050 on the host machine. 

## 3.1 deployment on Google Cloud Run
You can follow [this simple tutorial](https://medium.com/kunder/deploying-dash-to-cloud-run-5-minutes-c026eeea46d4) to deploy on google cloud run. (To summarise) Name the container in this format and push to the google container registry
```
docker build -f Dockerfile -t eu.gcr.io/{GOOGLE_PROJECT_ID}/{IMAGE_NAME}:{TAG} .

docker push eu.gcr.io/{GOOGLE_PROJECT_ID}/{IMAGE_NAME}:{TAG}

# Need to install or setup gcloud CLI for this. It more convienient using the web gui honestly)
gcloud run deploy dash_demo \
      --image=eu.gcr.io/{GOOGLE_PROJECT_ID}/{IMAGE_NAME}:{TAG} \
      --platform=managed \
      --region=us-central1 \  # GCP region
      --timeout=60 \          # Request timeout (max 900 secs)
      --concurrency=80 \      # Max request concurrency (default 80)
      --cpu=1 \               # Instance number of CPU's (default 1)
      --memory=256Mi \        # Instance memory (default 256Mi)
      --max-instances=10  \   # Max instances
      --allow-unauthenticated # Make service publicly available
```
# 4. Using the webpage
Note that this visualiser does not cover all of the settings of the PWM, but it can be useful viewing settings for the Base PWM for multiple instances (It is not updarted to include the dead-time controller)
Use the webpage to calculate the parameters required to achieve a specific PWM configuration from the IP. There are 2 methods for designing the PWM values:

1. Manually change the register values, viewing the waveform that is created and the parameters produced. The parameters produced by these set of register inputs should appear in the greyed out "calculate parameters" input boxes
2. Change the switch shown in the image below to input the high level parameter eg. frequency, phase etc. The register values in the now greyed out "manual registers" tab will update with the register values that closest produce the requested parameters. As there will be some rounding to produce these parameters, the actual produced frequency, phase, duty cycle etc. are described in the bottom right text. 
   
![The switch changes between manually entering](doc_asset/pwm_doc_image.png)

You can add up to 3 more PWM instances from the input section labeled "Select which figures to update and display". The radio dial on the top line selects which PWM instance changes in the parameters will apply to (and will change the parameters to display the currently loaded value).

**Settings** 

In the "Settings" tab you can set some parameters:
- Input frequency: This is the input clock frequency of the IP, by default 400 MHz and will change the frequency and resolution parameters
- Display periods: Change the number of PWM periods to display. Default is 3
- Decimal points: How many decimal points to round to for calculation displays
- Number of points on graph: Describes the resolution of the graph. By default, there are 5000 points on the grpaph

The visualisation assumes that all of the PWMs are already synchronised and ignores effects such as shadow register loading, control inputs, latency etc. 

There is a setting in waveform_funcs.py that I have not added an input for but can be changed manually - "account_startup_phase_waveform". This visualises what will happen in the event of SYNC_PHASE > CC0, the counter cycle directly after a new sync_phase value is loaded. The first waveform will be different than the rest because the waveform will not go high until the waveform AFTER the first CC0 occurs, which will be in the second counter cycle. 

**Known current bugs** 


```diff
- Looking at multiple output waveforms only functions correctly when all PWMs both have the same CTRH/ Frequency value (The X axis is chosen as the largest value at the moment)
- Changes to the manual parameters sometimes will glitch so the calculated parameter registers are one change behind
- (small issue) Manual parameter input boxes glitch to their previous values sometimes when you change them 
```

# 5. Components explained
The webpage uses open source python package [Dash](https://github.com/plotly/dash) to create interactive graphs. For styling it uses [Dash bootstrap components](https://github.com/facultyai/dash-bootstrap-components). If you are interested in learning dash I would reccomend this [youtube channel](https://www.youtube.com/@CharmingData/about)

> **main.py** is the entry point to the program, the layout of components is described in the app.layout object, instantiating objects that are described in **components.py** such as the graph object, settings inputs, information display etc. It 

> **callbacks.py** uses objects from component.py to create interactivity - updating the graph as inputs change. See [dash documentation](https://dash.plotly.com/basic-callbacks) for more info on callbacks
> 
> **pwm_funcs.py** contains functions for calculating the pwm parameters from register values, and for calculating the register values given certain parameters
> 
>**waveform_funcs.py** contains one function for updating the graph figure when updates are made, calculating the PWM output waveforms 


