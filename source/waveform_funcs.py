import pandas as pd
import numpy as np

account_startup_phase_waveform = "no"

def get_waveform_df(num_cycles,x_interval,CTRH, CC0,CC1, SYNPHS ):

    # 1 counter cycle, interpolated to contain x_interval points
    counter_cycle_x = np.trunc(np.linspace(SYNPHS,SYNPHS+CTRH, x_interval))

    # Make sure that SYNPHS is not > counter high or else will cause bugs
    counter_cycle_x[counter_cycle_x > CTRH] = counter_cycle_x[counter_cycle_x > CTRH] - CTRH -1


    # Find first incidence of CC0
    first_cc0_index = np.where(counter_cycle_x == CC0)[0]

    # Seperate calculation for first counter cycle because it may be different due to SYNC_PHS > CC0
    # This is disabled by defauly but you can add it back in by changing account_startup_phase_waveform to "yes"

    first_filter_cc0 = np.full(x_interval, False)
    if len(first_cc0_index) > 0:
        # Wait until first CC0 instance to do CC0 calculations
        first_filter_cc0[first_cc0_index[0]:] = counter_cycle_x[first_cc0_index[0]:] >= CC0

    # These comparisons create an array of booleans that can be used to filter other arrays
    filter_cc0 = counter_cycle_x >= CC0
    filter_cc1 = counter_cycle_x >= CC1

    first_cycle_y = np.empty(x_interval); first_cycle_y.fill(0)
    y_values = np.empty(x_interval); y_values.fill(0)

    # IF CC0>CC1, CC1 has no affect on output, only CC1
    # Output waveform is filtered by setting high all values >= CC0
    if CC0 >= CC1 :
        y_values[filter_cc0] = 1
        first_cycle_y[first_filter_cc0] = 1

    # CC0 must take priority over CC1 or Zero on a collision, Values > CC1 should be 0
    else:
        y_values[filter_cc0] = 1
        first_cycle_y[first_filter_cc0] = 1

        first_cycle_y[filter_cc1] = 0
        y_values[filter_cc1] = 0

    # If sync_phs > CTRH, The counter will be out of step and start counting up to its overflow
    if SYNPHS > CTRH:
        y_values.fill(0)
        first_cycle_y.fill(0)

    # X values for plotting
    x_waveform_values = np.linspace(SYNPHS,SYNPHS+CTRH*num_cycles, x_interval*num_cycles )

    # counter values as seen by the PWM module
    x_counter_values = np.tile(counter_cycle_x, num_cycles)


    if account_startup_phase_waveform == "yes":
      # Output waveform created by adding (first waveform output) + num_cycles * (Normal waveform output). 
      # This takes into account when you initialise a counter with a sync value > CC0
      _values = np.concatenate((first_cycle_y, np.tile(y_values, num_cycles-1)))
    elif account_startup_phase_waveform == "no":
      y_values = np.tile(y_values, num_cycles)

    return {'x_vals':x_waveform_values, 'y_vals': y_values, 'counter_vals' :x_counter_values}
