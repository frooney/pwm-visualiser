
def get_pwm_frequency(clockf_mhz, prescale, CTRH):
    # return PWM frequency in Khz
    if prescale is None:
        print("Error prescale should not be none")
        return 0
    if prescale == 0:
        # Calculation for DDR mode -> counter tick rate is effectively 2 * clockf
        return (2 * clockf_mhz * 1e3) / (((CTRH + 1) * (prescale + 1)))
    if prescale > 0:
        # Prescaled mode ->
        return (clockf_mhz * 1e3) / (((CTRH + 1) * (prescale)))

# returns PWM resolution (counter tick rate) in nanoseconds
# Due to DDR component, resolution at PRSC=0 is 2* clock period
def get_pwm_resolution(clockf_mhz, prescale):
    if prescale == 0:
        return (1000 * (prescale + 1)) / (2 * clockf_mhz)
    elif prescale > 0:
        return (1000 * prescale) / clockf_mhz
    else:
        return 0

# Use the frequency requested in kHz and calculate the required CTRH (level) value to  provide it
# Clockf is the master clock frequency in kHz
def get_CTRH_from_frequency(clockf, prescale, frequency):
    if frequency == 0:
        return [0,0]
    ctrh_desired = (2*clockf / ((prescale + 1) * (frequency/1000) )) - 1
    return [round(ctrh_desired), get_pwm_frequency(clockf, prescale, round(ctrh_desired)) ]

def get_CC0_from_duty(CTRH, duty, CC1):
    CC0 = CTRH + 1 - (duty/100) * (CTRH + 1)
    CC0 = round(CC0)
    return [CC0, get_duty_cycle(CTRH, CC0, CC1)]

# calculates the duty cycle based on CTRH, CC0, CC1 and returns it as a percentage
def get_duty_cycle(CTRH, CC0, CC1):
    duty_cycle = 0
    if CC1 > CTRH and CC0 < CTRH:
        duty_cycle = (CTRH+1 - CC0)/(CTRH+1)
    if CC1 <= CTRH and CC0 <= CTRH and CC1>CC0:
        duty_cycle = (CC1-CC0)/(CTRH+1)
    if CC1 <= CTRH and CC1 <= CTRH and CC1==CC0:
        duty_cycle = (CTRH + 1 - CC0) / (CTRH + 1)
    if CC1 <= CTRH and CC1 <= CTRH and CC1<CC0:
        duty_cycle = (CTRH + 1 - CC0) / (CTRH + 1)
    if CC0 == 0 and CC1 > CTRH:
        duty_cycle = 1
    if CC1 == 0 and CC0 > CTRH:
        duty_cycle = 0
    if CC0 > CTRH:
        duty_cycle = 0
    return duty_cycle*100

def get_SYNCPHS_from_phase(CTRH, phase):
    phase_inc_per_tick = 360/(CTRH + 1)
    sync_phs = phase / phase_inc_per_tick
    sync_phs_round = round(sync_phs)
    actual_phs = get_phase(CTRH, sync_phs_round)
    return [sync_phs_round, actual_phs]

def get_phase(CTRH, SYNC_PHS):
    return (SYNC_PHS*360)/(CTRH+1)



