import dash
import components
import pwm_funcs as pwm
from dash import html, Input, Output, State, ctx
from dash.exceptions import PreventUpdate
from datetime import datetime

import waveform_funcs as wave
import json
import plotly.express as px
import pandas as pd

graph_dict = {
    'data_reg': {'PRSC': 0, 'CTRH': 0, 'CC0': 0, 'CC1': 0, 'SYNC': 0},
    'attributes': {'freq', 'duty','phase', 'res'},
    'update_time': datetime.now()}
graph_dict = {'graph1': graph_dict, 'graph2': graph_dict, 'graph3': graph_dict, 'graph4': graph_dict}

# All callbacks are returned in a function - this is a workaround for defining callbacks in another file to Dash.dash app object

def get_callbacks(app):
    

    # Update the graph waveform when new data arrives (Data is stored in store-graph-coefficents shared object)
    @app.callback(
        Output("waveform1", 'figure'),
        Input('store_graph-coefficents', 'data'),
        Input('num_periods', 'value'),
        State('current_graph', 'value'),
        Input('display_graph', 'value'),
        State('graph_points', 'value'),
    )
    def set_graph1_waveform(graph_data, periods, current_graph, graphs_to_display, graph_points ):
        no_x_values = graph_points

        line_colors = ["#FF1C59", "#FF7F11", "#87B6A7","#136F63" ]
        fig = px.line(title="Visualised PWM output")
        list_graph_data = [0,0,0,0]
        highest_xvalues =[0,0,0,0] #Make the xAxis equal to the xAxis of the highest counter value figure
        for graphid in range(1, 5):
            if graphid in graphs_to_display:
                # Get register values for each graph if the display box is ticked
                register_values = graph_data[f'graph_{graphid}']['data_reg']
                this_graph_data = wave.get_waveform_df(periods, no_x_values, register_values['CTRH'], register_values['CC0'],
                                                  register_values['CC1'], register_values['SYNC'])
                list_graph_data[graphid-1] = this_graph_data

                # Store the largest X value in order to work out which X axis to use
                highest_xvalues[graphid-1] = list_graph_data[graphid-1]['x_vals'][no_x_values-1]

        max_xval = max(highest_xvalues)
        x_axis_index = highest_xvalues.index(max_xval)
        for graphid in range(1, 5):
            if graphid in graphs_to_display:
                fig.add_scatter(x=list_graph_data[x_axis_index]["x_vals"], y=list_graph_data[graphid-1]['y_vals'],
                                marker={"color": line_colors[graphid - 1]},
                                line={'dash': 'dash'})
        return fig

    # Greys out manual register input boxes in calculate parameters mode and vice versa
    @app.callback(
        Output('PRSC', 'disabled'),
        Output('CTRH', 'disabled'),
        Output('SYNC_PHS', 'disabled'),
        Output('CC0', 'disabled'),
        Output('CC1', 'disabled'),

        Output('param_PRSC', 'disabled'),
        Output('param_FRQ', 'disabled'),
        Output('param_DC', 'disabled'),
        Output('param_PHS', 'disabled'),
        Input('manual_or_calculated_param', 'value')
    )
    def change_input_mode(calculated_mode):
        if calculated_mode:
            return True,True,True,True,True, False,False, False, False
        else:
            return False,False,False,False,False, True, True, True, True

    @app.callback(
        Output('PRSC', 'value'),
        Output('CTRH', 'value'),
        Output('SYNC_PHS', 'value'),
        Output('CC0', 'value'),
        Output('CC1', 'value'),
        Input('current_graph', 'value'),
        Input('store_graph-coefficents', 'data'),
        Input('param_PRSC', 'value'),
        Input('param_FRQ', 'value'),
        Input('param_DC', 'value'),
        Input('param_PHS', 'value'),
        Input('manual_or_calculated_param', 'value')
    )
    def change_register_tab(current_graph,graphs_states,prsc,frq,dc,phs, in_calculated_mode):

      graph_key = f'graph_{current_graph}'
      graphs_state = graphs_states[graph_key]['data_reg']
      if ctx.triggered_id == "current_graph" and not in_calculated_mode:
          return graphs_state['PRSC'], graphs_state['CTRH'], graphs_state['SYNC'], graphs_state['CC0'], graphs_state['CC1'],

      return prsc, graphs_state['CTRH'], graphs_state['SYNC'], graphs_state['CC0'], graphs_state['CC1'],

    # This callback ticks the "display graph" checkbox when a graph is displayed
    @app.callback(
        Output('display_graph', 'value'),
        Input('current_graph', 'value'),
        State('display_graph', 'value'),
    )
    def display_graph_on_update_graph(current_graph, graphs_displayed):
        if current_graph not in graphs_displayed:
            graphs_displayed.append(current_graph)
        return graphs_displayed

    @app.callback(
        Output('param_PRSC', 'value'),
        Output('param_FRQ', 'value'),
        Output('param_DC', 'value'),
        Output('param_PHS', 'value'),
        Input('current_graph', 'value'),
        Input('store_graph-coefficents', 'data'),
        Input('manual_or_calculated_param', 'value'),
        Input('param_PRSC', 'value'),
        Input('param_FRQ', 'value'),
        Input('param_DC', 'value'),
        Input('param_PHS', 'value'),
    )
    def update_calc_parameters_in_manual_mode(selected_graph, graph_data, in_calculate_mode, PRSC, FRQ,DC, PHS):
        graph_key = f'graph_{selected_graph}'
        this_graph_data = graph_data[graph_key]
        attributes = this_graph_data['attributes']
        prsc = this_graph_data['data_reg']['PRSC']
        if ctx.triggered_id == "current_graph" and in_calculate_mode:
            return prsc, attributes['freq'], attributes['duty'],attributes['phase']
        if in_calculate_mode:
            return PRSC, FRQ, DC,PHS
        else:
            selected_prsc = this_graph_data['data_reg']['PRSC']
            return selected_prsc, attributes['freq'], attributes['duty'],attributes['phase']
        


    @app.callback(
        Output('display_frequency', 'children'),
        Output('display_duty', 'children'),
        Output('display_phase', 'children'),
        Output('display_resolution', 'children'),
        Output('calculated_registers', 'data'),
        Output('store_graph-coefficents', 'data'),
        Input('manual_or_calculated_param', 'value'),

        Input('decimal_points', 'value'),
        Input('clockf', 'value'),

        Input('PRSC', 'value'), # Manual register input boxe values
        Input('CTRH', 'value'),
        Input('SYNC_PHS', 'value'),
        Input('CC0', 'value'),
        Input('CC1', 'value'),

        Input('param_PRSC', 'value'), # Set register 
        Input('param_FRQ', 'value'),
        Input('param_DC', 'value'),
        Input('param_PHS', 'value'),
        State('current_graph', 'value'),
        State('store_graph-coefficents', 'data'),

    )
    def calculate_param_labels(calculated_mode, dp, clkf, m_prsc, m_ctrh, m_sync, m_cc0, m_cc1,
                               c_prsc, c_freq, c_dc, c_phs, current_graph, graphs_state):
        graph_data_object = graphs_state
        graph_key = f'graph_{current_graph}'

        freq_template = "PWM Frequency (kHz): "
        duty_template = "Duty cycle (%): "
        phase_template = "Phase (°): "
        resolution_template = "Resolution (ns): "

        if not calculated_mode:

            if None in [m_prsc, m_ctrh, m_sync, m_cc0, m_cc1]:
                return [dash.no_update] * 6
            freq = round(pwm.get_pwm_frequency(clkf, m_prsc, m_ctrh),dp)
            duty = round(pwm.get_duty_cycle(m_ctrh, m_cc0, m_cc1),dp)
            phase = round(pwm.get_phase(m_ctrh, m_sync),dp)
            resolution =round(pwm.get_pwm_resolution(clkf, m_prsc),dp)

            freq_div = freq_template + str(freq)
            duty_div = duty_template + str(duty)
            phase_div = phase_template + str(phase)
            resolution_div = resolution_template + str(resolution)

            save_graph_data = {'data_reg': dict(PRSC=m_prsc, CTRH=m_ctrh, CC0=m_cc0, CC1=m_cc1, SYNC=m_sync),
                               'attributes': dict(freq=freq, duty=duty, phase=phase, resolution=resolution)
                               }

            graph_data_object[graph_key] = save_graph_data
            return freq_div, duty_div, phase_div, resolution_div, dash.no_update, graph_data_object
        else:
            registers = graph_data_object[graph_key]

            if None in [c_prsc, c_freq]:
                freq_div = freq_template + "Enter a value for prescale and frequency"
            else:
                freq_ctrh = pwm.get_CTRH_from_frequency(clkf, c_prsc, c_freq)
                freq_div = freq_template + str(round(freq_ctrh[1], dp))
                registers['data_reg']['CTRH'] = freq_ctrh[0]
                registers['attributes']['freq'] = freq_ctrh[1]
            if c_dc is None:
                duty_div = duty_template + "Enter value for duty cycle "
            else:
                duty_cc0 = pwm.get_CC0_from_duty(freq_ctrh[0], c_dc, 0)
                registers['data_reg']['CC0'] = duty_cc0[0]
                duty_div = duty_template + str(round(duty_cc0[1], dp))
                registers['attributes']['duty'] = duty_cc0[1]

            if None in [c_freq, c_phs]:
                phase_div = phase_template + "Enter a value for frequency and phase"
            else:
                phase_syncphs = pwm.get_SYNCPHS_from_phase(freq_ctrh[0], c_phs)
                phase_div = phase_template + str(round(phase_syncphs[1], dp))
                registers['data_reg']['SYNC'] = phase_syncphs[0]
                registers['attributes']['phase'] = phase_syncphs[1]

            if c_prsc is None:
                resolution_div = resolution_template + "Enter a value for prescale"
            else:
                resolution_div = resolution_template + str(round(pwm.get_pwm_resolution(clkf, c_prsc), dp))
                registers['attributes']['resolution'] = round(pwm.get_pwm_resolution(clkf, c_prsc), dp)
            registers['data_reg']['PRSC'] = c_prsc
            graph_data_object[graph_key] = registers

            return freq_div, duty_div, phase_div, resolution_div, registers, graph_data_object
