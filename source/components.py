import dash
from dash import Dash, html, dcc, Input, Output

import dash_bootstrap_components as dbc
import plotly.express as px
from datetime import datetime

load_to_graph_buttons_reg = dbc.ButtonGroup([
    dbc.Button("Load to graph 1", id="load_graph1", outline=True, color="primary", n_clicks=0),
    dbc.Button("Load to graph 2", id="load_graph2", outline=True, color="primary", n_clicks=0),
])


register_input_tab = html.Div([

    dbc.InputGroup([
        dbc.InputGroupText("Prescale"),
        dbc.Input(id='PRSC', value=0, type='number', min=0, step=1,
                  placeholder="Enter PRSC"),
    ], className="mb-2", ),
    dbc.InputGroup([
        dbc.InputGroupText("Counter High"),
        dbc.Input(id='CTRH', value=99, type='number', min=9, step=1,
                  placeholder="Enter CTRH Value"),
    ], className="mb-2", ),
    dbc.InputGroup([
        dbc.InputGroupText("Sync Phase"),
        dbc.Input(id='SYNC_PHS', value=0, type='number', min=0, step=1,
                  placeholder="Enter SYNC_PHS"),
    ], className="mb-2", ),
    dbc.InputGroup([
        dbc.InputGroupText("Compare 0"),
        dbc.Input(id='CC0', value=25, type='number', min=0, step=1,
                  placeholder="Enter a positive value"),
    ], className="mb-2", ),
    dbc.InputGroup([
        dbc.InputGroupText("Compare 1   "),
        dbc.Input(id='CC1', value=75, type='number', min=0, step=1,
                  placeholder="Enter CC1>"
                  ),
    ], className="mb-2", ),
], className = 'mt-2')


register_accordian = html.Div([dcc.Store(id="manual_registers", storage_type='session'),
                               dbc.Label(""), # Add a space between tabs and accordian
                               dbc.Accordion([
                                   dbc.AccordionItem(
                                       [
                                           html.P(
                                               "Counter prescaling ratio. Affects counter resolution. Note that prescaling starts at 2x input clock frequency"),
                                           dbc.Input(id='PRSC', value=0, type='number', min=0, step=1,
                                                     placeholder="Enter a positive value"),
                                       ],
                                       title="Prescale", id="PRSC_accrd"
                                   ),
                                   dbc.AccordionItem(
                                       [
                                           html.P("Value that the counte rolls over at. Must be greater than 8"),
                                           dbc.Input(id='CTRH', value=0, type='number', min=9, step=1,
                                                     placeholder="Enter a positive value"),
                                       ],
                                       title="Counter High", id="CTRH_accrd"
                                   ),
                                   dbc.AccordionItem(
                                       [
                                           html.P('''Value that is loaded into the counter when sync input pulse is sent. 
                                Can be used to created a phase shift between multiple instances'''),
                                           dbc.Input(id='SYNC_PHS', value=0, type='number', min=0, step=1,
                                                     placeholder="Enter a positive value"),
                                       ],
                                       title="Sync phase", id="syn_phs_accrd"
                                   ),
                                   dbc.AccordionItem(
                                       [
                                           html.P(
                                               "When the counter reaches this value, the output waveform will toggle HIGH"),
                                           dbc.Input(id='CC0', value=0, type='number', min=0, step=1,
                                                     placeholder="Enter a positive value"),
                                       ],
                                       title="Compare 0", id="CC0_accrd",
                                   ),
                                   dbc.AccordionItem(
                                       [
                                           html.P('''When the counter reaches this value, or zero, 
                                the output waveform will toggle LOW (both are over-ridden by compare 0)'''),
                                           dbc.Input(id='CC1', value=0, type='number', min=0, step=1,
                                                     placeholder="Enter a positive value"
                                                     )

                                       ],
                                       title="Compare 1 ", id="CC1_accrd"
                                   ),
                                   dbc.AccordionItem(
                                       [
                                           html.P('''This toggle inverts the effects of counter = compare 0,
                             compare 1, zero on the output waveform'''),
                                           dbc.Checkbox(id='inv_check', label="Invert", value=0)
                                       ],
                                       title="Invert", id="inv_accrd"
                                   )

                               ], start_collapsed=True, flush=True),
                               load_to_graph_buttons_reg
                               ], )

pwm_calculations = html.Div(dbc.ListGroup([
    dbc.ListGroupItem(
        [
            html.P("Frequency"),
        ], color="primary"
    ),
    dbc.ListGroupItem(
        [
            html.P("Duty cycle"),
        ], color="primary"
    ),
    dbc.ListGroupItem(
        [
            html.P('''Phase'''),
        ], color="primary"
    ),
    dbc.ListGroupItem(
        [
            html.P("Resolution"),
        ], color="primary"
    ),
], flush=False)
)

param_graph_button = dbc.ButtonGroup([
    dbc.Button("Load to graph 1", id="param_load_graph1", outline=True, color="primary", n_clicks=0),
    # dbc.Button("Load to graph 2", id="param_load_graph2", outline=True, color="primary", n_clicks=0),
])

load_to_graph_toggle = html.Div(
    [
        dbc.Checklist(
            options=[
                {"label": "Load calculated parameters to graph", "value": 1},
            ],
            value=0,
            id="load-calculated-param",
            inline=True,
            switch=True,
            className="m-2",
        ),
    ]
)
manual_or_calc_toggle = html.Div(
    [
        dbc.Checklist(
            options=[
                {"label": "Input high level parameters eg phase, frequency, duty cycle ", "value": 1},
            ],
            value=0,
            id="manual_or_calculated_param",
            inline=True,
            switch=True,
            className="m-2",
        ),
    ]
)


calculate_param_tab = html.Div([
    dcc.Store(id="calculated_registers", storage_type='session'),
    html.Br(),
    dbc.InputGroup([
        dbc.InputGroupText("Set Prescale: "),
        dbc.Input(placeholder="Prescale value", id="param_PRSC", type="number", min=0, step=1),
    ], className="mb-1", ),
    dbc.InputGroup([
        dbc.InputGroupText("Frequency (kHz): "),
        dbc.Input(placeholder="PWM frequency in Khz", id="param_FRQ", type="number", min=0),
    ], className="mb-1", ),
    dbc.InputGroup([
        dbc.InputGroupText("Duty cycle (%): "),
        dbc.Input(placeholder="Duty Cycle 0-100 %", id="param_DC", type="number", min=0, max=100),
    ], className="mb-1", ),
    dbc.InputGroup([
        dbc.InputGroupText("Phase (°): "),
        dbc.Input(placeholder="Phase in degrees", id="param_PHS", type="number", min=0, max=360 ),
    ], className="mb-1",),
    manual_or_calc_toggle,
])

graph_dict = {
    'data_reg': {'PRSC': 0, 'CTRH': 99, 'CC0': 25, 'CC1': 75, 'SYNC': 0},
    'attributes': {'prsc': 0, 'freq' : 55, 'duty' : 50,'phase' : 0, 'res': 1.25},
    'update_time': datetime.now()}
init_graph_data = {'graph_1': graph_dict, 'graph_2': graph_dict, 'graph_3': graph_dict, 'graph_4': graph_dict}

select_graph = html.Div(
    [
        dbc.Label("Select which figures to update and diplay", className = "m-2"),
        dbc.RadioItems(
            options=[
                {"label": "Update 1", "value": 1},
                {"label": "Update 2", "value": 2},
                {"label": "Update 3", "value": 3},
                {"label": "Update 4", "value": 4},
            ],
            value=1,
            id="current_graph",
            inline=True,
            className = "m-2"
        ),
    dcc.Store(id="store_graph-coefficents", storage_type='memory', data=init_graph_data),
    ],
)
display_figures = html.Div(
    [
        dbc.Checklist(
            options=[
                {"label": "Display 1", "value": 1},
                {"label": "Display 2", "value": 2},
                {"label": "Display 3", "value": 3},
                {"label": "Display 4", "value": 4},
            ],
            value=[1],
            id="display_graph",
            inline=True,
            className = "m-2"
        ),
    ],
)

figure_options_card = dbc.Card([
    select_graph, display_figures],
    className = "mb-2"
)

display_attributes = dbc.Card([
        dbc.ListGroup([
            dbc.ListGroupItem("Frequency: 0 Khz", id="display_frequency"),
            dbc.ListGroupItem("Duty cycle: 0 %", id="display_duty"),
            dbc.ListGroupItem("Phase: 0°", id="display_phase"),
            dbc.ListGroupItem("Resolution: 1.25 ns", id="display_resolution"),
        ])
    ], className = "mb-4")

settings_tab = html.Div([
    html.Br(),
    dbc.InputGroup([
        dbc.InputGroupText("Input frequency (MHz)"),
        dbc.Input(placeholder="Enter input frequency", id="clockf", type="number",value = 400, min=0),
    ], className="mb-3", ),
    dbc.InputGroup([
        dbc.InputGroupText("Display periods"),
        dbc.Input(placeholder="Number of PWM periods to display", id="num_periods", type="number", min=0, value = 3),
    ], className="mb-3", ),
    dbc.InputGroup([
        dbc.InputGroupText("Decimal points"),
        dbc.Input(placeholder="Amount of rounding used ", id="decimal_points", type="number", min=0, max=12, step=1, value=8),
    ], className="mb-3",),
    dbc.InputGroup([
        dbc.InputGroupText("Number of points on graph"),
        dbc.Input(placeholder="Number of points to display on the graph", id="graph_points", type="number", min=0, step=1, value=5000),
    ], className="mb-3",)
])

manual_reg_and_settings = dbc.Tabs(
    [dbc.Tab(register_input_tab, label="Manual registers", tab_id="manual_reg"),
    dbc.Tab(settings_tab, label="Settings", tab_id="settings"),
],  id =  'mode_tabs')


param_standalone_tab =dbc.Tabs(
    dbc.Tab(calculate_param_tab, label="Calculate parameters", tab_id="calc_param"),
)

select_register_mode_tab = dbc.Tabs(
    [dbc.Tab(register_input_tab, label="Manual parameters", tab_id="manual_reg"),
    dbc.Tab(calculate_param_tab, label="Calculate parameters", tab_id="calc_param"),
    dbc.Tab(settings_tab, label="Settings", tab_id="settings"),
],  id =  'mode_tabs')

pwm_waveform1 = dcc.Graph(
    id="waveform1", style={'width': '170vh', 'height': '60vh'}
)
pwm_waveform2 = dcc.Graph(
    id="waveform2"
)




