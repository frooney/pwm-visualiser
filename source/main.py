#import polars as pl
import dash
from dash import html, dcc
import dash_bootstrap_components as dbc
import components as cmp
from dash_bootstrap_templates import load_figure_template
from callbacks import get_callbacks


load_figure_template("cyborg")

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

server = app.server
app.title = "PWM API Visualiser"

app.layout = dbc.Container([
    dbc.Row([
        dbc.Col([
            html.H1("PWM Visualiser", style={'textAlign': 'center'})
        ], width=12)
    ]),
        dbc.Row([
                    dbc.Col([
                        cmp.pwm_waveform1
                    ], width={'size': 12, "offset": 0, 'order': 0} )
                ]),
        dbc.Row([
                    dbc.Col([
                        dbc.Card( cmp.manual_reg_and_settings)
                    ], className='mt-3', width={'size': 4, "offset": 0, 'order': 1}, ),
                    dbc.Col([
                        dbc.Card(cmp.param_standalone_tab)
                    ], className='mt-3', width={'size': 4, "offset": 0, 'order': 1}, ),

                    dbc.Col([
                        html.Div(id = "attribute_display",
                                 children=[cmp.figure_options_card, cmp.display_attributes])
                    ], width={'size': 4, "offset": 0, 'order': 1}, className='mt-3'),
                ],),
], fluid=True, style={"height": "100vh"},
)
get_callbacks(app)

if __name__ == '__main__':
    app.run_server(debug=True)